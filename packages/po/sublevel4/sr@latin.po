# Serbian/Latin messages for debian-installer.
# Copyright (C) 2010-2012 Software in the Public Interest, Inc.
# Copyright (C) 2008 THE cp6Linux'S COPYRIGHT HOLDER
# This file is distributed under the same license as the debian-installer package.
# Karolina Kalic <karolina@resenje.org>, 2010-2012.
# Janos Guljas <janos@resenje.org>, 2010-2012.
# Veselin Mijušković <veselin.mijuskovic@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-14 20:02+0000\n"
"PO-Revision-Date: 2012-01-22 19:41+0100\n"
"Last-Translator: Karolina Kalic <karolina@resenje.org>\n"
"Language-Team: Serbian <debian-l10n-serbian@lists.debian.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Pootle 1.1.0\n"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:1001
#, no-c-format
msgid "!! ERROR: %s"
msgstr "!! GREŠKA: %s"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:2001
msgid "KEYSTROKES:"
msgstr "PRITISCI:"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:3001
#, no-c-format
msgid "'%c'"
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:4001
msgid "Display this help message"
msgstr "Prikaži ovu pomoćnu poruku"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:5001
msgid "Go back to previous question"
msgstr "Nazad na prethodno pitanje"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:6001
msgid "Select an empty entry"
msgstr "Izaberi prazan unos"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:7001
#, no-c-format
msgid "Other choices are available with '%c' and '%c'"
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:8001
#, no-c-format
msgid "Previous choices are available with '%c'"
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:9001
#, no-c-format
msgid "Next choices are available with '%c'"
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:12001
#, no-c-format
msgid "Prompt: '%c' for help, default=%d> "
msgstr "Pitaj: '%c' -- pomoć, podrazumevano=%d> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:13001
#, no-c-format
msgid "Prompt: '%c' for help> "
msgstr "Pitaj: „%c“ za pomoć> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:14001
#, no-c-format
msgid "Prompt: '%c' for help, default=%s> "
msgstr "Pitaj: '%c' -- pomoć, podrazumevano=%s> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:15001
msgid "[Press enter to continue]"
msgstr "[Pritisni „Enter“ za nastavak]"

#. Type: select
#. Description
#. :sl4:
#: ../cdebconf.templates:1001
msgid "Interface to use:"
msgstr "Interfejs za upotrebu:"

#. Type: select
#. Description
#. :sl4:
#: ../cdebconf.templates:1001
msgid ""
"Packages that use debconf for configuration share a common look and feel. "
"You can select the type of user interface they use."
msgstr ""
"Paketi koji koriste debconf za konfiguraciju dele zajednički izgled i način "
"rada. Možete izabrati tip interfejsa za upotrebu."

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:2001
msgid "None"
msgstr "Nijedan"

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:2001
msgid "'None' will never ask you any question."
msgstr "„None“ nikada neće postaviti pitanje."

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:3001
msgid "Text"
msgstr "Tekst"

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:3001
msgid "'Text' is a traditional plain text interface."
msgstr "„Tekst“ je tradicionalni tekstualni interfejs."

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:4001
msgid "Newt"
msgstr "Newt"

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:4001
msgid "'Newt' is a full-screen, character based interface."
msgstr "„Newt“ je interfejs baziran na karakterima, preko celog ekrana."

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:5001
msgid "GTK"
msgstr "GTK"

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:5001
msgid ""
"'GTK' is a graphical interface that may be used in any graphical environment."
msgstr ""
"„GTK“ je grafički interfejs koji može da se koristi u bilo kom grafičkom "
"okruženju."

#. Type: note
#. Description
#. Main menu entry.
#. :sl4:
#: ../templates:1001
#, fuzzy
#| msgid "Make the system bootable"
msgid "Make this ChromeOS board bootable"
msgstr "Omogućavanje pokretanja sistema"

#. Type: note
#. Description
#. Progress bar title.
#. :sl4:
#: ../templates:2001
#, fuzzy
#| msgid "Making the system bootable"
msgid "Making this ChromeOS board bootable"
msgstr "Omogućavanje startovanja sistema"

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:3001
#, fuzzy
#| msgid "Installing the Cobalt boot loader"
msgid "Installing tools to manage ChromeOS boot images"
msgstr "Instaliranje pokretačkog programa „Cobalt“"

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:4001
#, fuzzy
#| msgid "Checking partitions"
msgid "Checking ChromeOS kernel partitions"
msgstr "Proveravanje particije"

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:5001
msgid "Updating initramfs"
msgstr ""

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:6001
msgid "Building a boot image"
msgstr ""

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:7001
#, fuzzy
#| msgid "Generating boot image on disk..."
msgid "Writing the boot image to disk"
msgstr "Generisanje pokretačke slike na diske..."

#. Type: error
#. Description
#. :sl4:
#: ../templates:9001
#, fuzzy
#| msgid "No root partition found"
msgid "No usable ChromeOS kernel partition is found"
msgstr "Nijedna root particija nije pronađena"

#. Type: error
#. Description
#. :sl4:
#: ../templates:9001
#, fuzzy
#| msgid ""
#| "An error was returned while trying to install the kernel into the target "
#| "system."
msgid "An error was returned while searching for a ChromeOS kernel partition."
msgstr "Nastala je greška pri pokušaju instalacije kernela na ciljni sistem."

#. Type: error
#. Description
#. :sl4:
#: ../templates:10001
msgid "Cannot build a boot image"
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../templates:10001
#, fuzzy
#| msgid ""
#| "An error was returned while trying to install the kernel into the target "
#| "system."
msgid "An error was returned while building a boot image."
msgstr "Nastala je greška pri pokušaju instalacije kernela na ciljni sistem."

#. Type: error
#. Description
#. :sl4:
#: ../templates:11001
#, fuzzy
#| msgid "Generating boot image on disk..."
msgid "Cannot write boot image to disk"
msgstr "Generisanje pokretačke slike na diske..."

#. Type: error
#. Description
#. :sl4:
#: ../templates:11001
#, fuzzy
#| msgid "An error occurred while writing the changes to the disks."
msgid "An error was returned while writing the boot image to disk."
msgstr "Došlo je do greške prilikom zapisivanja izmena na diskove."

#. Type: boolean
#. Description
#. :sl4:
#: ../templates:12001
msgid "Reconfigure initramfs policies?"
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#: ../templates:12001
msgid ""
"Could not generate a small enough boot image for this board. Usually this "
"can be resolved by including less modules in the initramfs."
msgstr ""

#. Type: text
#. Description
#. This item is a progress bar heading when the system configures
#. some flashable memory used by many embedded devices
#. :sl4:
#: ../flash-kernel-installer.templates:1001
msgid "Configuring flash memory to boot the system"
msgstr "Konfigurisanje fleš memorije da pokreće sistem"

#. Type: text
#. Description
#. This item is a progress bar heading when an embedded device is
#. configured so it will boot from disk
#. :sl4:
#: ../flash-kernel-installer.templates:2001
msgid "Making the system bootable"
msgstr "Omogućavanje startovanja sistema"

#. Type: text
#. Description
#. This is "preparing the system" to flash the kernel and initrd
#. on a flashable memory
#. :sl4:
#: ../flash-kernel-installer.templates:3001
msgid "Preparing the system..."
msgstr "Pripremanje sistema..."

#. Type: text
#. Description
#. This is a progress bar showing up when the system
#. write the kernel to the flashable memory of the embedded device
#. :sl4:
#: ../flash-kernel-installer.templates:4001
msgid "Writing the kernel to flash memory..."
msgstr "Kopiranje kernela na fleš memoriju..."

#. Type: text
#. Description
#. This is a progress bar showing up when the system generates a
#. special boot image on disk for some embedded device so they
#. can boot.
#. :sl4:
#: ../flash-kernel-installer.templates:5001
msgid "Generating boot image on disk..."
msgstr "Generisanje pokretačke slike na diske..."

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. This item is a menu entry for a step where the system configures
#. the flashable memory used by many embedded devices
#. (writing the kernel and initrd to it)
#. :sl4:
#: ../flash-kernel-installer.templates:6001
msgid "Make the system bootable"
msgstr "Omogućavanje pokretanja sistema"

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid "Install GRUB?"
msgstr "Instalirati GRUB?"

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid ""
"GRUB 2 is the next generation of GNU GRUB, the boot loader that is commonly "
"used on i386/amd64 PCs. It is now also available for ${ARCH}."
msgstr ""
"GRUB 2 je sledeća generacija GNU GRUB-a, pokretački program koji se najčešće "
"koristi na i386/amd64 PC-jevima. Sada je takođe dostupan za ${ARCH}."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid ""
"It has interesting new features but is still experimental software for this "
"architecture. If you choose to install it, you should be prepared for "
"breakage, and have an idea on how to recover your system if it becomes "
"unbootable. You're advised not to try this in production environments."
msgstr ""
"Ima zanimljive karakteristike, ali je još uvvek eksperimentalan softver za "
"ovu arhitekturu. Ako izaberete da ga instalirate, trebate biti spremni za "
"kvarove, i znati kako da povratite vaš sistem ako postane nebutabilan. "
"Savetuje se da ovo ne pokušavate na okruženjima koja se koriste za "
"produkciju."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
#, fuzzy
#| msgid "Failed to mount CD-ROM"
msgid "Failed to mount ${PATH}"
msgstr "Neuspelo moniranje CD-ROM-a"

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
#, fuzzy
#| msgid "Mounting the proc file system on /target/proc failed."
msgid "Mounting the ${FSTYPE} file system on ${PATH} failed."
msgstr "Montiranje proc fajl sistema na /target/proc nije uspelo."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001 ../nobootloader.templates:2001
msgid "Warning: Your system may be unbootable!"
msgstr "Upozorenje: vaš sistem možda neće moći da se startuje!"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Failed to mount /target/proc"
msgstr "Neuspelo montiranje /target/proc"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Mounting the proc file system on /target/proc failed."
msgstr "Montiranje proc fajl sistema na /target/proc nije uspelo."

#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001 ../nobootloader.templates:4001
msgid "Setting firmware variables for automatic boot"
msgstr "Postavljanje promenljive firmvera za automatsko startovanje"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Some variables need to be set in the Genesi firmware in order for your "
"system to boot automatically.  At the end of the installation, the system "
"will reboot.  At the firmware prompt, set the following firmware variables "
"to enable auto-booting:"
msgstr ""
"Neke promenljive treba da budu postavljene u Genesi firmver da bi se vaš "
"sistem automatski startovao. Na kraju instalacije sistem će se restartovati. "
"Na promptu firmvera postavite sledeće promenljive da bi omogućili automatsko "
"startovanje:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"You will only need to do this once.  Afterwards, enter the \"boot\" command "
"or reboot the system to proceed to your newly installed system."
msgstr ""
"Ovo treba da uradite samo jednom. Posle toga, unesite komandu „boot“ ili "
"restartujte sitem da bi nastavili rad sa novoinstaliranim sistemom."

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Alternatively, you will be able to boot the kernel manually by entering, at "
"the firmware prompt:"
msgstr ""
"U suprotnom, možete startovati kernel ručno unoseći na prompt firmvera:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"Some variables need to be set in CFE in order for your system to boot "
"automatically. At the end of installation, the system will reboot. At the "
"firmware prompt, set the following variables to simplify booting:"
msgstr ""
"Neke promenljive treba da budu postavljene u CFE-u da bi se vaš sistem "
"automatski pokrenuo. Na kraju instalacije sistem će se restartovati. Na "
"promptu firmvera postavite sledeće promenljive da bi pojednostavili "
"pokretanje:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"You will only need to do this once. This enables you to just issue the "
"command \"boot_debian\" at the CFE prompt."
msgstr ""
"Ovo treba da uradite samo jednom. To vam omogućuje da na CFE promptu samo "
"zadate komandu „boot_debian“."

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"If you prefer to auto-boot on every startup, you can set the following "
"variable in addition to the ones above:"
msgstr ""
"Ako želite da se prilikom svakog startovanja automatski pokrene vaš Debian "
"sistem, možete postaviti sledeću promenljivu pored gore navedenih:"

#. Type: text
#. Description
#. eg. Virtual disk 1 (xvda)
#. :sl4:
#: ../partman-base.templates:62001
#, no-c-format
msgid "Virtual disk %s (%s)"
msgstr "Virtuelni disk %s (%s)"

#. Type: text
#. Description
#. eg. Virtual disk 1, partition #1 (xvda1)
#. :sl4:
#: ../partman-base.templates:63001
#, no-c-format
msgid "Virtual disk %s, partition #%s (%s)"
msgstr "Virtuelni disk %s, particija #%s (%s)"

#. Type: text
#. Description
#. :sl4:
#. Note to translators: Please keep your translations of this string below
#. a 65 columns limit (which means 65 characters in single-byte languages)
#: ../partman-basicfilesystems.templates:58001
msgid "acls - support POSIX.1e Access Control List"
msgstr "acls - podrška za POSIX.1e Access Control List"

#. Type: text
#. Description
#. :sl4:
#. Note to translators: Please keep your translations of this string below
#. a 65 columns limit (which means 65 characters in single-byte languages)
#: ../partman-basicfilesystems.templates:59001
msgid "shortnames - only use the old MS-DOS 8.3 style filenames"
msgstr "kratki nazivi - koristiti stare MS-DOS 8.3 nazive fajlova"

#. Type: text
#. Description
#. :sl4:
#: ../partman-cros.templates:1001
#, fuzzy
#| msgid "Checking partitions"
msgid "ChromeOS kernel partition"
msgstr "Proveravanje particije"

#. Type: text
#. Description
#. Short form of ChromeOS/ChromiumOS. Keep short.
#. :sl4:
#: ../partman-cros.templates:2001
msgid "CrOS"
msgstr ""

#. Type: boolean
#. Description
#. Users are familiar with boot methods as necessary to boot d-i itself.
#. :sl4:
#: ../partman-cros.templates:3001
msgid ""
"No usable ChromeOS kernel partition is found. At least one such partition is "
"necessary for your machine to be bootable with ChromeOS' verified boot "
"methods (with CTRL+D or CTRL+U). These partitions must reside on the same "
"physical disk as either the root or the boot partition. Two 512 MB "
"partitions should be fine for most installations."
msgstr ""

#. Type: boolean
#. Description
#. Users are familiar with boot methods as necessary to boot d-i itself.
#. :sl4:
#: ../partman-cros.templates:3001
msgid ""
"If you cannot boot into the installed system in another way (like legacy "
"boot with CTRL+L), continuing the installation would result in an unbootable "
"machine."
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../partman-nbd.templates:1001
msgid "Configure the Network Block Device"
msgstr "Konfiguriši mrežni uređaj"

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:2001
msgid "NBD configuration action:"
msgstr "Konfigurisanje mreže:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:2001
msgid "There are currently ${NUMBER} devices connected."
msgstr "Trenutno je ${NUMBER} grupa povezano."

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:3001
msgid "Network Block Device server:"
msgstr "Server mrežnog uređaja:"

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:3001
msgid ""
"Please enter the host name or the IP address of the system running nbd-"
"server."
msgstr "Unesite ime hosta ili IP adresu sistema na kome radi „nbd-server“."

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:4001
msgid "Name for NBD export"
msgstr ""

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:4001
msgid ""
"Please enter the NBD export name needed to access nbd-server. The name "
"entered here should match an existing export on the server."
msgstr ""

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:5001
msgid "Network Block Device device node:"
msgstr "Čvor mrežnog uređaja:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:5001
msgid ""
"Please select the NBD device node that you wish to connect or disconnect."
msgstr ""
"Izaberite čvor mrežnog uređaja koji želite da se povežete ili da otkačite."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:6001
msgid "Failed to connect to the NBD server"
msgstr "Neuspelo konektovanje na server mrežnog uređaja"

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:6001
#, fuzzy
#| msgid ""
#| "Connecting to the nbd-server failed. Please ensure that the host and the "
#| "port or the name which you entered are correct, that the nbd-server "
#| "process is running on that host and port (or using that name), that the "
#| "network is configured correctly, and retry."
msgid ""
"Connecting to the nbd-server failed. Please ensure that the host and the "
"export name which you entered are correct, that the nbd-server process is "
"running on that host, that the network is configured correctly, and retry."
msgstr ""
"Povezivanje na „nbd-server“ nije uspelo. Potvrdite da su host i port ili ime "
"koje ste uneli ispravni, da „nbd-server“ radi na tom hostu i postu (ili "
"koristi to ime), da je mreća pravilno konfigurisana i probajte ponovo."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:7001
msgid "No more Network Block Device nodes left"
msgstr "Nema više slobodnih mrežnih čvorova"

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:7001
msgid ""
"Either all available NBD device nodes are in use or something went wrong "
"with the detection of the device nodes."
msgstr ""
"Ili su svi dostupni mrećni čvorovi u upotrebi ili je nešto pošlo naopako pri "
"detekciji čvorova uređaja."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:7001
msgid ""
"No more NBD device nodes can be configured until a configured one is "
"disconnected."
msgstr ""
"Nijedan više čvor mrežnog uređaja ne može biti konfigurisan dok se neki "
"konfigurisan ne isključi."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:8001
msgid "No connected Network Block Device nodes were found"
msgstr "Nisu pronađeni povezani čvorovi mrežnog uređaja"

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:8001
msgid ""
"There are currently no Network Block Device nodes connected to any server. "
"As such, you can't disconnect any of them."
msgstr ""
"Trenutno nema povezanih mrežnih čvorova ni za jedan server. Tako da ne "
"možete da isključite ni jedan od njih."

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinite form
#: ../partman-nbd.templates:9001
msgid "Connect a Network Block Device"
msgstr "Poveži mrežni uređaj"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinite form
#: ../partman-nbd.templates:10001
msgid "Disconnect a Network Block Device"
msgstr "Isključiti mrežni uređaj"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinite form
#: ../partman-nbd.templates:11001
msgid "Finish and return to the partitioner"
msgstr "Završi i vrati se na meni za particionisanje"

#. Type: text
#. Description
#. :sl4:
#: ../partman-target.templates:3001
msgid ""
"In order to start your new system, a so called boot loader is used.  It is "
"installed in a boot partition.  You must set the bootable flag for the "
"partition.  Such a partition will be marked with \"${BOOTABLE}\" in the main "
"partitioning menu."
msgstr ""
"Da biste pokrenuli vaš novi sistem, morate koristiti tzv. pokretački "
"program. On je instaliran na but particiji. Morate postaviti butabilnu "
"zastavicu za tu particiju. Takva particija će biti označena kao "
"„${BOOTABLE}“ u meniju za particionisanje."
